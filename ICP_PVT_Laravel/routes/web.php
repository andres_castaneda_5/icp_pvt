<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('register', 'Auth\RegisterController@register')->name('register');
Route::get('/401', function () {
    return view('401');
});

Route::get('/admin', ['middleware' => 'admin', function () {
    return view('/admin.index');
}])->name('admin');

Route::resource('usuario', 'UserController');
Route::get('user/{id}/edit', 'UserController@password');
Route::post('user/{id}/edit', 'UserController@updatePassword');
Route::resource('pozo', 'PozoController');
Route::resource('muestra', 'MuestraController');
Route::resource('densimetro', 'DensimetroController');
Route::resource('viscosimetro', 'ViscosimetroController');
Route::resource('gasometro', 'GasometroController');
Route::resource('celda', 'CeldaController');

Route::get('/home', 'HomeController@index')->name('home');
