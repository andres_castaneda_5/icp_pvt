<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;

class Densimetro extends Model implements AuditableContract, UserResolver
{
    use Auditable;

    public $timestamps = false;
    public $fillable = ['codigo', 'modelo'];
    
    public static function resolve(){
        return Auth::check() ? Auth::User()->getAuthIdentifier() : null;
    }

    public function muestras()
    {
        return $this->hasMany('App\DensimetrosMuestras', 'id_densimetro', 'id');
    }
}
