<?php

namespace App\Http\Controllers;

use App\Densimetro;
use Illuminate\Http\Request;

class DensimetroController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $densimetros = Densimetro::orderBy('id', 'DESC')->paginate(5);
        return view('densimetro.index', ['densimetros' => $densimetros]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('densimetro.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Densimetro::create($request->all());
        return redirect()->action('DensimetroController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $densimetro = Densimetro::find($id);
        return view('densimetro.show', ['densimetro' => $densimetro]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $densimetro = Densimetro::find($id);
        return view('densimetro.edit', ['densimetro' => $densimetro]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $densimetro = Densimetro::find($id);
        $densimetro->update($request->all());
        return redirect()->action('DensimetroController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $densimetro = Densimetro::find($id);
        $densimetro->delete();
        return redirect()->action('DensimetroController@index');
    }
}
