<?php

namespace App\Http\Controllers;

use App\Pozo;
use Illuminate\Http\Request;

class PozoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $nombre = $request->get('nombre');
        $pozos = Pozo::orderBy('nombre', 'ASC')->nombre($nombre)->paginate(7);
        return view('pozo.index', ['pozos' => $pozos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pozo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Pozo::create($request->all());
        flash('El pozo se ha creado correctamente')->important();
        return redirect()->action('PozoController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pozo = Pozo::find($id);
        return view('pozo.show', ['pozo' => $pozo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pozo = Pozo::find($id);
        return view('pozo.edit', ['pozo' => $pozo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pozo = Pozo::find($id);
        $pozo->update($request->all());
        flash('El pozo se ha actualizado correctamente')->important();
        return redirect()->action('PozoController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pozo = Pozo::find($id);
        $pozo->delete();
        flash('El pozo se ha eliminado correctamente')->important();
        return redirect()->action('PozoController@index');
    }
}
