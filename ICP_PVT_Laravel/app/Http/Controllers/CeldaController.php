<?php

namespace App\Http\Controllers;

use App\Celda;
use Illuminate\Http\Request;

class CeldaController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $celdas = Celda::orderBy('id', 'DESC')->paginate(5);
        return view('celda.index', ['celdas' => $celdas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('celda.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Celda::create($request->all());
        return redirect()->action('CeldaController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $celda = Celda::find($id);
        return view('celda.show', ['celda' => $celda]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $celda = Celda::find($id);
        return view('celda.edit', ['celda' => $celda]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $celda = Celda::find($id);
        $celda->update($request->all());
        return redirect()->action('CeldaController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $celda = Celda::find($id);
        $celda->delete();
        return redirect()->action('CeldaController@index');
    }
}
