<?php

namespace App\Http\Controllers;

use App\Gasometro;
use Illuminate\Http\Request;

class GasometroController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gasometros = Gasometro::orderBy('id', 'DESC')->paginate(5);
        return view('gasometro.index', ['gasometros' => $gasometros]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gasometro.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gasometro::create($request->all());
        return redirect()->action('GasometroController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gasometro = Gasometro::find($id);
        return view('gasometro.show', ['gasometro' => $gasometro]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gasometro = Gasometro::find($id);
        return view('gasometro.edit', ['gasometro' => $gasometro]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gasometro = Gasometro::find($id);
        $gasometro->update($request->all());
        return redirect()->action('GasometroController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gasometro = Gasometro::find($id);
        $gasometro->delete();
        return redirect()->action('GasometroController@index');
    }
}
