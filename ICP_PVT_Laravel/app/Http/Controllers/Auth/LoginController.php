<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login()
    {
        $credentials = $this->validate(request(), [
            'correo' => 'required|string',
            'password' => 'required|string'

        ]);
        if (Auth::attempt($credentials)) {
            if (auth()->user()->rol == 'admin') {
                return redirect()->route('admin');
            } else {
                return redirect()->route('home');
            }
        }
        return back()
            ->withErrors(['correo' => 'El correo o la contraseña que ingresaste no coinciden con nuestros registros.'])
            ->withErrors(['password' => 'El correo o la contraseña que ingresaste no coinciden con nuestros registros.'])
            ->withInput(request(['correo'], ['password']));
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }
}
