<?php

namespace App\Http\Controllers;

use App\Muestra;
use App\Pozo;
use Illuminate\Http\Request;

class MuestraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nombre = $request->get('nombre');
        $muestras = Muestra::orderBy('id', 'DESC')->nombre($nombre)->paginate(7);
        $pozos = Pozo::all();
        return view('muestra.index', ['muestras' => $muestras], ['pozos' => $pozos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pozos = Pozo::orderBy('nombre', 'ASC')->get();
        return view('muestra.create', ['pozos' => $pozos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Muestra::create($request->all());
        return redirect()->action('MuestraController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $muestra = Muestra::find($id);
        return view('muestra.show', ['muestra' => $muestra]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pozos = Pozo::orderBy('nombre', 'ASC')->get();
        $muestra = Muestra::find($id);
        return view('muestra.edit', ['muestra' => $muestra], ['pozos' => $pozos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $muestra = Muestra::find($id);
        $muestra->update($request->all());
        return redirect()->action('MuestraController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $muestra = Muestra::find($id);
        $muestra->delete();
        return redirect()->action('MuestraController@index');
    }
}
