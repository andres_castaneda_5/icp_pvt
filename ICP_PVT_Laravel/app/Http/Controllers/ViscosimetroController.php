<?php

namespace App\Http\Controllers;

use App\Viscosimetro;
use Illuminate\Http\Request;

class ViscosimetroController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $viscosimetros = Viscosimetro::orderBy('id', 'DESC')->paginate(5);
        return view('viscosimetro.index', ['viscosimetros' => $viscosimetros]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('viscosimetro.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Viscosimetro::create($request->all());
        return redirect()->action('ViscosimetroController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $viscosimetro = Viscosimetro::find($id);
        return view('viscosimetro.show', ['viscosimetro' => $viscosimetro]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $viscosimetro = Viscosimetro::find($id);
        return view('viscosimetro.edit', ['viscosimetro' => $viscosimetro]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $viscosimetro = Viscosimetro::find($id);
        $viscosimetro->update($request->all());
        return redirect()->action('ViscosimetroController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $viscosimetro = Viscosimetro::find($id);
        $viscosimetro->delete();
        return redirect()->action('ViscosimetroController@index');
    }
}
