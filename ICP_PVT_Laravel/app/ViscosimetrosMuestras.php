<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViscosimetrosMuestras extends Model
{
    public $timestamps = false;
    public $fillable = [
        'fecha', 'presion', 'viscosidad', 'pcv', 'temperatura', 'ciclo', 'desviacion', 'observaciones', 'id_usuario', 'id_viscosimetro', 'id_muestra'
    ];

    public function usuarios()
    {
        return $this->hasMany('App\User', 'id_usuario', 'id');
    }

    public function muestras()
    {
        return $this->hasMany('App\Muestra', 'id_muestra', 'id');
    }

    public function viscosimetros()
    {
        return $this->hasMany('App\Viscosimetro', 'id_viscosimetro', 'id');
    }
}
