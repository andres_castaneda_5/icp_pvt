<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GasometrosMuestras extends Model
{
    public $timestamps = false;
    public $fillable = [
        'fecha', 'presion', 'temperatura', 'volumen1', 'volumen2', 'peso1', 'peso2', 'peso3', 'observaciones', 'id_usuario', 'id_gasometro', 'id_muestra'
    ];

    public function usuarios()
    {
        return $this->hasMany('App\User', 'id_usuario', 'id');
    }

    public function muestras()
    {
        return $this->hasMany('App\Muestra', 'id_muestra', 'id');
    }

    public function gasometros()
    {
        return $this->hasMany('App\Gasometro', 'id_gasometro', 'id');
    }
}
