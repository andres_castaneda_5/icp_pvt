<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;

class Muestra extends Model implements AuditableContract, UserResolver
{
    use Auditable;

    public $timestamps = false;
    public $fillable = ['submission', 'nombre', 'tipo_muestra', 'formacion_productora', 'profundidad', 'presion', 'volumen', 'temperatura', 'contenido_agua', 'id_pozo'];

    public function scopeNombre($query, $nombre)
    {
        if ($nombre) {
            return $query->where('nombre', 'LIKE', "%$nombre%");
        }
    }

    public static function resolve()
    {
        return Auth::check() ? Auth::User()->getAuthIdentifier() : null;
    }

    public function pozo()
    {
        return $this->hasMany('App\Pozo', 'id_pozo', 'id');
    }

    public function densimetros()
    {
        return $this->hasMany('App\DensimetrosMuestras', 'id_muestra', 'id');
    }

    public function viscosimetros()
    {
        return $this->hasMany('App\ViscosimetrosMuestras', 'id_muestra', 'id');
    }

    public function gasometros()
    {
        return $this->hasMany('App\GasometrosMuestras', 'id_muestra', 'id');
    }

    public function celdas()
    {
        return $this->hasMany('App\CeldasMuestras', 'id_muestra', 'id');
    }
}
