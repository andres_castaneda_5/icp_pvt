<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;

class Pozo extends Model implements AuditableContract, UserResolver
{
    use Auditable;

    public $timestamps = false;
    public $fillable = ['submission', 'nombre', 'tipo_muestra', 'punto_muestreo', 'formacion_productora', 'profundidad'];

    public function scopeNombre($query, $nombre)
    {
        if ($nombre) {
            return $query->where('nombre', 'LIKE', "%$nombre%");
        }
    }

    public static function resolve()
    {
        return Auth::check() ? Auth::User()->getAuthIdentifier() : null;
    }


    public function muestras()
    {
        return $this->hasMany('App\Muestra', 'id_pozo', 'id');
    }
}
