<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DensimetrosMuestras extends Model
{
    public $timestamps = false;
    public $fillable = [
        'fecha', 'temperatura', 'densidad1', 'densidad2', 'densidad3', 'densidad4', 'densidad5', 'densidad6', 'densidad_prom', 'api', 'agua', 'observaciones', 'id_usuario', 'id_densimetro', 'id_muestra'
    ];

    public function usuarios()
    {
        return $this->hasMany('App\User', 'id_usuario', 'id');
    }

    public function muestras()
    {
        return $this->hasMany('App\Muestra', 'id_muestra', 'id');
    }

    public function densimetros()
    {
        return $this->hasMany('App\Densimetro', 'id_densimetro', 'id');
    }
}
