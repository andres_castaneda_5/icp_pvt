<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CeldasMuestras extends Model
{
    public $timestamps = false;
    public $fillable = [
        'fecha', 'presion', 'temperatura', 'volumen', 'observaciones', 'id_usuario', 'id_celda', 'id_muestra'
    ];

    public function usuarios()
    {
        return $this->hasMany('App\User', 'id_usuario', 'id');
    }

    public function muestras()
    {
        return $this->hasMany('App\Muestra', 'id_muestra', 'id');
    }

    public function celdas()
    {
        return $this->hasMany('App\Celda', 'id_celda', 'id');
    }
}
