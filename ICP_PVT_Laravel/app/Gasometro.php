<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;

class Gasometro extends Model implements AuditableContract, UserResolver
{
    use Auditable;

    public $timestamps = false;
    public $fillable = ['codigo', 'modelo'];

    public function muestras()
    {
        return $this->hasMany('App\GasometrosMuestras', 'id_gasometro', 'id');
    }
}
