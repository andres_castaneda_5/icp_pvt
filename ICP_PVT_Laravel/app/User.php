<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;

class User extends Authenticatable implements AuditableContract, UserResolver
{
    use Auditable;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['documento', 'nombre', 'correo', 'password', 'rol'];

    public function scopeNombre($query, $nombre)
    {
        if ($nombre) {
            return $query->where('nombre', 'LIKE', "%$nombre%");
        }
    }

    public static function resolve()
    {
        return Auth::check() ? Auth::User()->getAuthIdentifier() : null;
    }

    public function densimetros()
    {
        return $this->hasMany('App\DensimetrosMuestras', 'id_usuario', 'id');
    }

    public function viscosimetros()
    {
        return $this->hasMany('App\ViscosimetrosMuestras', 'id_usuario', 'id');
    }

    public function gasometros()
    {
        return $this->hasMany('App\GasometrosMuestras', 'id_usuario', 'id');
    }

    public function celdas()
    {
        return $this->hasMany('App\CeldasMuestras', 'id_usuario', 'id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password'];
}
