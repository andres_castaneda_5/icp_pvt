@extends('layouts.auth')

@section('content')
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <center>
                        <h4>Agregar pozo</h4>
                    </center>
                </div>
                <div class="card-body">
                    <form action="{{action('PozoController@store')}}" method="post">
                        {{csrf_field()}}                        
                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">
                                Nombre Pozo:
                            </label>
                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control" name="nombre">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="submission" class="col-md-4 col-form-label text-md-right">
                                Submission:
                            </label>
                            <div class="col-md-6">
                                <input id="submission" type="number" class="form-control" name="submission">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tipo_muestra" class="col-md-4 col-form-label text-md-right">
                                Tipo Muestra:
                            </label>
                            <div class="col-md-6">
                                <select id="tipo_muestra" class="custom-select" name="tipo_muestra">
                                    <option selected>Presione para seleccionar...</option>
                                    <option value="Fondo">Fondo</option>
                                    <option value="Superficie">Superficie</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="punto_muestreo" class="col-md-4 col-form-label text-md-right">
                                Punto Muestreo:
                            </label>
                            <div class="col-md-6">
                                <input id="punto_muestreo" type="text" class="form-control" name="punto_muestreo">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="formacion_productora" class="col-md-4 col-form-label text-md-right">
                                Formación Productora:
                            </label>
                            <div class="col-md-6">
                                <input id="formacion_productora" type="text" class="form-control" name="formacion_productora">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="profundidad" class="col-md-4 col-form-label text-md-right">
                                Profundidad:
                            </label>
                            <div class="col-md-6">
                                <input id="profundidad" type="number" class="form-control" name="profundidad">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-3 offset-md-4">
                                <button type="submit" class="btn btn-light btn-block">
                                    {{ __('Guardar') }}
                                </button>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-light btn-block" href="{{action('PozoController@index')}}" role="button">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection