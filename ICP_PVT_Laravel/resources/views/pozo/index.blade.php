@extends('layouts.app')
@section('content')
@include('flash::message')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-5">
                <center>
                    <strong>
                        <h1>Gestionar Pozos</h1>
                    </strong>
                </center>
            </div>
            <div class="col-md-1 offset-3">
                <a href="{{action('PozoController@create')}}">
                    <img src="../images/add.png" width="35" height="35" alt="">
                </a>
            </div>
            <div class="col-md-3">
                <form action="pozo" method="GET" class="flexsearch--form">
                    <div class="input-group mb-3">
                        <input type="search" class="flexsearch--imput form-control" name="nombre" placeholder="Buscar">
                        <div class="input-group-append">
                            <button class="btn" type="sumbit" id="buscar">
                                <img src="../images/search.png" alt="Buscar" width="20" height="20">
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">
                        <center>Submission</center>
                    </th>
                    <th scope="col">
                        <center>Nombre Pozo</center>
                    </th>
                    <th scope="col">
                        <center>Tipo Muestra</center>
                    </th>
                    <th scope="col">
                        <center>Formación Productora</center>
                    </th>
                    <th scope="col">
                        <center>Punto Muestreo</center>
                    </th>
                    <th scope="col">
                        <center>Profundidad</center>
                    </th>
                    <th scope="col">
                        <center>Funciones</center>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($pozos as $pozo)
                <tr>
                    <th>
                        <center>{{$pozo->submission}}</center>
                    </th>
                    <td>
                        <center>{{$pozo->nombre}}</center>
                    </td>
                    <td>
                        <center>{{$pozo->tipo_muestra}}</center>
                    </td>
                    <td>
                        <center>{{$pozo->formacion_productora}}</center>
                    </td>
                    <td>
                        <center>{{$pozo->punto_muestreo}}</center>
                    </td>
                    <td>
                        <center>{{$pozo->profundidad}}</center>
                    </td>
                    <td>
                        @if(Auth()->user()->rol == 'admin')
                        <div class="row">
                            <div class="col-md-4 offset-2">
                                <form action="{{action('PozoController@edit', ['id'=>$pozo->id])}}" method="GET">
                                    <input type="image" src="../images/edit.png" width="20" height="20" role="button">
                                </form>
                            </div>
                            <div class="col-md-2">
                                <form action="{{action('PozoController@destroy', ['id'=>$pozo->id])}}" method="POST">
                                    {{csrf_field()}} {{method_field('DELETE')}}
                                    <input type="image" src="../images/delete.png" alt="Borrar" onclick="return ConfirmDelete()" width="20" height="20" role="button">
                                </form>
                            </div>
                        </div>
                        @else
                        <div class="row">
                            <div class="col">
                                <center>
                                    <form action="{{action('PozoController@edit', ['id'=>$pozo->id])}}" method="GET">
                                        <input type="image" src="../images/edit.png" width="20" height="20" role="button">
                                    </form>
                                </center>
                            </div>
                        </div>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!!$pozos->render()!!}
    </div>
</div>
@endsection