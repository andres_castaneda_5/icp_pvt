@extends('layouts.app')
@section('content')
@include('flash::message')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-5">
                <center>
                    <strong>
                        <h1>Gestionar Usuarios</h1>
                    </strong>
                </center>
            </div>
            <div class="col-md-1 offset-3">
                <a href="/register" role="button"><img src="../images/add-user.png" width="35" height="35" alt=""></a>
            </div>
            <div class="col-md-3">
                <form action="usuario" method="GET" class="flexsearch--form">
                    <div class="input-group mb-3">
                        <input type="search" class="flexsearch--imput form-control" name="nombre" placeholder="Buscar">
                        <div class="input-group-append">
                            <button class="btn" type="sumbit" id="buscar">
                                <img src="../images/search.png" alt="Buscar" width="20" height="20">
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">
                        <center>Documento</center>
                    </th>
                    <th scope="col">
                        <center>Nombre</center>
                    </th>
                    <th scope="col">
                        <center>Correo</center>
                    </th>
                    <th>
                        <center>Tipo Usuario</center>
                    </th>
                    <th scope="col">
                        <center>Funciones</center>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($usuarios as $usuario)
                @if(Auth()->user()->id != $usuario->id)
                <tr>
                    <th scope="col">
                        <center>{{$usuario->documento}}</center>
                    </th>
                    <td scope="col">
                        <center>{{$usuario->nombre}}</center>
                    </td>
                    <td scope="col">
                        <center>{{$usuario->correo}}</center>
                    </td>
                    <td scope='col'>
                        @if($usuario->rol == 'admin')
                        <center>Administrador</center>
                        @elseif($usuario->rol == 'user')
                        <center>Operario</center>
                        @endif
                    </td>
                    <td scope="col">
                        <div class="row">
                            <div class="col-sm-2 offset-3">
                                <center>
                                    <a href="{{action('UserController@password', $usuario->id)}}"><img src="../images/password.png" width="20" height="20" alt=""></a>
                                </center>
                            </div>
                            <div class="col-sm-2">
                                <form action="{{action('UserController@edit', ['id'=>$usuario->id])}}" method="GET">
                                    <center>
                                        <input type="image" src="../images/edit.png" width="20" height="20" role="button">
                                    </center>
                                </form>
                            </div>
                            <div class="col-sm-2">
                                <form action="{{action('UserController@destroy', ['id'=>$usuario->id])}}" method="POST">
                                    {{csrf_field()}} {{method_field('DELETE')}}
                                    <center>
                                        <input type="image" src="../images/delete.png" alt="Borrar" onclick="return ConfirmDelete()" width="20" height="20" role="button">
                                    </center>
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col">
                <center>
                    {!!$usuarios->render()!!}
                </center>
            </div>

        </div>
    </div>
</div>
@endsection