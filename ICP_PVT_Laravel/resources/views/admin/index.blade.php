@extends('layouts.app')

@section('content')
<br>
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>
                    <center>Área PVT</center>
                </h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <center>
                            <a href="/usuario"><img src="../images/users.png" width="100" height="100" alt=""></a>
                        </center>
                    </div>
                    <div class="col">
                        <center>
                            <a href="/pozo"><img src="../images/well.png" width="100" height="90" alt=""></a>
                        </center>
                    </div>
                    <div class="col">
                        <center>
                            <a href="/muestra"><img src="../images/muestra.png" width="90" height="86" alt=""></a>
                        </center>
                    </div>
                    <div class="col">
                        <center>
                            <a href="/equipo"><img src="../images/equipo.png" width="90" height="85" alt=""></a>
                        </center>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <center>
                            <a class="btn btn-light btn-block" href="/usuario">Manejo de Usuarios</a>
                        </center>
                    </div>
                    <div class="col">
                        <center>
                            <a class="btn btn-light btn-block" href="/pozo">Manejo de Pozos</a>
                        </center>
                    </div>
                    <div class="col">
                        <center>
                            <a class="btn btn-light btn-block" href="/muestra">Manejo de Muestras</a>
                        </center>
                    </div>
                    <div class="col">
                        <center>
                            <a class="btn btn-light btn-block" href="/equipo">Manejo de Equipos</a>
                        </center>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col">
                        <center>
                            <a href="/usuario"><img src="../images/lab.png" width="85" height="85" alt=""></a>
                        </center>
                    </div>
                    <div class="col">
                        <center>
                            <a href=""><img src="../images/historial.png" width="85" height="85" alt=""></a>
                        </center>
                    </div>
                    <div class="col">
                        <center>
                            <a href=""><img src="../images/test.png" width="90" height="90" alt=""></a>
                        </center>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <center>
                            <a class="btn btn-light btn-block" href="/usuario">Manejo de Análisis</a>
                        </center>
                    </div>
                    <div class="col">
                        <center>
                            <a class="btn btn-light btn-block" href="" aria-disabled="true">Historial de Movimientos</a>
                        </center>
                    </div>
                    <div class="col">
                        <center>
                            <a class="btn btn-light btn-block" href="" aria-disabled="true">Actividades del día</a>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection