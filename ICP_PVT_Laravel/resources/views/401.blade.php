@extends('layouts.auth')

@section('content')

<div class="container">
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    <div class="row justify-content-center">
        <div class="col-md-7">
            <center>
                <h1>ERROR 401 - ¡ACCESO NO AUTORIZADO!</h1>
            </center>
        </div>
    </div>
    <hr style="width: 70%"/>

    <div class="row justify-content-center">
        <div class="col-md-6">
            <center>
                <h3>Lo sentimos, su petición no ha sido procesada.</h3>
            </center>
            <a href="/home">Volver</a>
        </div>
    </div>
</div>
@endsection