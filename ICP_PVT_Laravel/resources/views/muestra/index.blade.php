@extends('layouts.app')
@section('content')
@include('flash::message')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-5">
                <center>
                    <strong>
                        <h1>Gestionar Muestras</h1>
                    </strong>
                </center>
            </div>
            <div class="col-md-1 offset-3">
                <a href="{{action('MuestraController@create')}}"><img src="../images/add.png" width="35" height="35" alt=""></a>
            </div>
            <div class="col-md-3">
                <form action="muestra" method="GET" class="flexsearch--form">
                    <div class="input-group mb-3">
                        <input type="search" class="flexsearch--imput form-control" name="nombre" placeholder="Buscar">
                        <div class="input-group-append">
                            <button class="btn" type="sumbit" id="buscar">
                                <img src="../images/search.png" alt="Buscar" width="20" height="20">
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">
                        <center>Submission</center>
                    </th>
                    <th scope="col">
                        <center>Nombre</center>
                    </th>
                    <th scope="col">
                        <center>Pozo</center>
                    </th>
                    <th scope="col">
                        <center>Tipo Muestra</center>
                    </th>
                    <th scope="col">
                        <center>Formación Productora</center>
                    </th>
                    <th scope="col">
                        <center>Profundidad</center>
                    </th>
                    <th scope="col">
                        <center>Presión</center>
                    </th>
                    <th scope="col">
                        <center>Volumen</center>
                    </th>
                    <th scope="col">
                        <center>Temperatura</center>
                    </th>
                    <th scope="col">
                        <center>%SW</center>
                    </th>
                    <th scope="col">
                        <center>Funciones</center>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($muestras as $muestra)
                <tr>
                    <th>
                        <center>{{$muestra->submission}}</center>
                    </th>
                    <td>
                        <center>{{$muestra->nombre}}</center>
                    </td>
                    <td>
                        @foreach($pozos as $pozo)
                        @if ($pozo->id == $muestra->id_pozo)
                        <center>{{$pozo->nombre}}</center>
                        @endif
                        @endforeach
                    </td>
                    <td>
                        <center>{{$muestra->tipo_muestra}}</center>
                    </td>
                    <td>
                        <center>{{$muestra->formacion_productora}}</center>
                    </td>
                    <td>
                        <center>{{$muestra->profundidad}}</center>
                    </td>
                    <td>
                        <center>{{$muestra->presion}}</center>
                    </td>
                    <td>
                        <center>{{$muestra->volumen}}</center>
                    </td>
                    <td>
                        <center>{{$muestra->temperatura}}</center>
                    </td>
                    <td>
                        <center>{{$muestra->contenido_agua}}</center>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-4 offset-2">
                                <form action="{{action('MuestraController@edit', ['id'=>$muestra->id])}}" method="GET">
                                    <input type="image" src="../images/edit.png" width="20" height="20" role="button">
                                </form>
                            </div>
                            <div class="col-md-3">
                                <form action="{{action('MuestraController@destroy', ['id'=>$muestra->id])}}" method="POST">
                                    {{csrf_field()}} {{method_field('DELETE')}}
                                    <input type="image" src="../images/delete.png" alt="Borrar" width="20" height="20" role="button">
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!!$muestras->render()!!}
    </div>
</div>

@endsection