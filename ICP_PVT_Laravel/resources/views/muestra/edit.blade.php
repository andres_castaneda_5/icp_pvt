@extends('layouts.auth')

@section('content')
<br>
<br>
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Editar Muestra') }}</div>
                <div class="card-body">
                    <form action="{{action('MuestraController@update', ['id'=>$muestra->id])}}" method="post">
                        {{csrf_field()}}
                        {{method_field('PUT')}}

                        <div class="form-group row">
                            <label for="codigo" class="col-md-4 col-form-label text-md-right">
                                Código:
                            </label>
                            <div class="col-md-6">
                                <input id="codigo" type="text" class="form-control" name="codigo" value="{{$muestra->codigo}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tipo_muestra" class="col-md-4 col-form-label text-md-right">
                                Tipo Muestra:
                            </label>
                            <div class="col-md-6">
                                <select id="tipo_muestra" class="custom-select" name="tipo_muestra">
                                    <option selected>Presione para seleccionar...</option>
                                    <option value="Fondo">Fondo</option>
                                    <option value="Superficie">Superficie</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="presion" class="col-md-4 col-form-label text-md-right">
                                Presión:
                            </label>
                            <div class="col-md-6">
                                <input id="presion" type="text" class="form-control" name="presion" value="{{$muestra->presion}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="temperatura" class="col-md-4 col-form-label text-md-right" value="{{$muestra->codigo}}">
                                Temperatura:
                            </label>
                            <div class="col-md-6">
                                <input id="temperatura" type="number" class="form-control" name="temperatura" value="{{$muestra->temperatura}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="preofundidad" class="col-md-4 col-form-label text-md-right">
                                Profundidad:
                            </label>
                            <div class="col-md-6">
                                <input id="profundidad" type="text" class="form-control" name="profundidad" value="{{$muestra->profundidad}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="punto_muestreo" class="col-md-4 col-form-label text-md-right">
                                Punto Muestreo:
                            </label>
                            <div class="col-md-6">
                                <input id="punto_muestreo" type="text" class="form-control" name="punto_muestreo" value="{{$muestra->punto_muestreo}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="id_pozo" class="col-md-4 col-form-label text-md-right">
                                Pozo Muestreo:
                            </label>
                            <div class="col-md-6">
                                <select id="id_pozo" class="custom-select" name="id_pozo">
                                    <option selected>Presione para seleccionar...</option>
                                    @foreach($pozos as $pozo)
                                    <option value="{{$pozo->id}}">{{$pozo->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-3 offset-md-4">
                                <button type="submit" class="btn btn-light btn-block">
                                    {{ __('Guardar') }}
                                </button>
                            </div>

                            <div class="col-md-3">
                                <a class="btn btn-light btn-block" href="{{action('MuestraController@index')}}" role="button">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection