<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ver facultad</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th scope="col" colspan="4">
                    <center>
                        <h3>Facultad</h3>
                    </center>
                </th>
            </tr>
            <tr>
                <th scope="col">
                    <center>Código</center>
                </th>
                <th scope="col">
                    <center>Nombre</center>
                </th>
                <th scope="col">
                    <center>Decano</center>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <center>{{$facultad->codigo}}</center>
                </td>
                <td>
                    <center>{{$facultad->nombre}}</center>
                </td>
                <td>
                    <center>{{$facultad->decano}}</center>
                </td>
            </tr>
        </tbody>
    </table>
    <br><br>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="row" colspan="4">
                    <center>
                        <h4>Docentes</h4>
                    </center>
                </th>
            </tr>
            <tr>
                <th scope="row">
                    <center>Documento</center>
                </th>
                <th scope="row">
                    <center>Nombre</center>
                </th>
                <th scope="row">
                    <center>Profesión</center>
                </th>
                <th scope="row">
                    <center>Funciones</center>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($facultad->docentes as $docente)
            <tr>
                <td>
                    <center>{{$docente->documento}}</center>
                </td>
                <td>
                    <center>{{$docente->nombre}}</center>
                </td>
                <td>
                    <center>{{$docente->profesion}}</center>
                </td>
                <td>
                    <form action="{{action('DocenteController@destroy',['id'=>$docente->id])}}" method="POST">
                        {{csrf_field()}} {{method_field('DELETE')}}
                        <center>
                            <a class="btn btn-outline-dark" href="{{action('DocenteController@edit', ['id'=>$docente->id])}}" role="button">Editar</a>
                            <input type="submit" class="btn btn-outline-dark" value="Eliminar">
                        </center>
                    </form>
                </td>
            </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <center>
                        <a class="btn btn-dark" href="{{action('DocenteController@create', ['id_fac'=>$facultad->id])}}" role="button">Crear docente</a>
                    </center>
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <a class="btn btn-outline-dark" href="{{action('FacultadController@index')}}" role="button">Volver</a>
</body>

</html>