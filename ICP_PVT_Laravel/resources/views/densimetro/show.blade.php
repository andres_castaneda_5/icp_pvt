@extends('layouts.auth')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-9 shadow p-3 mb-5 bg-white rounded">
        <div class="row">
            <div class="col">
                <center>
                    <strong>
                        <h2>Densimetro</h2>
                    </strong>
                </center>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-4">
                <center>
                    <strong>
                        Código
                    </strong>
                </center>
            </div>
            <div class="col-sm-4">
                <center>
                    <strong>
                        Modelo
                    </strong>
                </center>
            </div>
            <div class="col-sm-4">
                <center>
                    <strong>
                        Funciones
                    </strong>
                </center>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <center>
                    {{$densimetro->codigo}}
                </center>
            </div>
            <div class="col-sm-4">
                <center>
                    {{$densimetro->modelo}}
                </center>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col">
                        <form action="{{action('DensimetroController@edit', ['id'=>$densimetro->id])}}" method="GET">
                            <center>
                                <input type="image" src="../images/edit.png" width="25" height="25" role="button">
                            </center>
                        </form>
                    </div>
                    <div class="col">
                        <form action="{{action('DensimetroController@destroy',['id'=>$densimetro->id])}}" method="POST">
                            {{csrf_field()}} {{method_field('DELETE')}}
                            <center>
                                <input type="image" src="../images/delete.png" alt="Borrar" width="25" height="25" role="button">
                            </center>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <br>
        <div class="row">
            <div class="col">
                <center>
                    <strong>
                        <h2>Análisis realizados</h2>
                    </strong>
                </center>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col">
                <center>
                    <strong>Pozo</strong>
                </center>
            </div>
            <div class="col">
                <center>
                    <strong>Fecha</strong>
                </center>
            </div>
            <div class="col">
                <center>
                    <strong>Temperatura</strong>
                </center>
            </div>
            <div class="col">
                <center>
                    <strong>Densidad Promedio</strong>
                </center>
            </div>
            <div class="col">
                <center>
                    <strong>API</strong>
                </center>
            </div>
            <div class="col">
                <center>
                    <strong>Nivel Agua</strong>
                </center>
            </div>
            <div class="col">
                <center>
                    <strong>Observaciones</strong>
                </center>
            </div>
        </div>
        <br>
        @foreach($densimetro->muestras as $muestra)
        <div class="row">
            <div class="col">
                <center>{{$muestra->codigo}}</center>
            </div>
            <div class="col">
                <center>2020-01-12</center>
            </div>
            <div class="col">
                <center>75°</center>
            </div>
            <div class="col">
                <center>15</center>
            </div>
            <div class="col">
                <center>1%</center>
            </div>
            <div class="col">
                <center>0.4%</center>
            </div>
            <div class="col">
                <center>-</center>
            </div>
        </div>
        @endforeach
        <br>
        <div class="row">
            <div class="col-sm-2">
                <center>
                    <a class="btn btn-light" href="{{action('DensimetroController@index')}}" role="button">Volver</a>
                </center>
            </div>
        </div>
    </div>
</div>

@endsection