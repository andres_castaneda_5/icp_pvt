@extends('layouts.auth')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5 shadow p-3 mb-5 bg-white rounded">
            <div class="row">
                <div class="col">
                    <img src="../images/register.png" class="float-right" width="35" height="35">
                </div>
                <div class="col">
                    <center>
                        <h2>Registrar</h2>
                    </center>
                </div>
                <div class="col"></div>
            </div>
            <hr>
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <label for="documento" class="offset-md-1">
                    <strong>Documento:</strong>
                </label>
                <div class="col-md-10 offset-md-1">
                    <input id="documento" type="number" class="form-control @error('documento') is-invalid @enderror" name="documento" value="{{ old('documento') }}" required autocomplete="documento" autofocus placeholder="Digite el documento de identidad">
                    @error('documento')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <label for="nombre" class="offset-md-1">
                    <strong>Nombre:</strong>
                </label>
                <div class="col-md-10 offset-md-1">
                    <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus placeholder="Digite el nombre completo">
                    @error('nombre')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <label for="correo" class="offset-md-1">
                    <strong>Correo electrónico:</strong>
                </label>
                <div class="col-md-10 offset-md-1">
                    <input id="correo" type="email" class="form-control @error('correo') is-invalid @enderror" name="correo" value="{{ old('correo') }}" required autocomplete="correo" autofocus placeholder="Digite el correo electrónico">
                    @error('correo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <label for="password" class="offset-md-1">
                    <strong>Contraseña:</strong>
                </label>
                <div class="col-md-10 offset-md-1">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Digite la contraseña">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <label for="password-confirm" class="offset-md-1">
                    <strong>Confirmar contraseña:</strong>
                </label>
                <div class="col-md-10 offset-md-1">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirme la contraseña">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <label for="rol" class="offset-md-1">
                    <strong>Tipo de usuario:</strong>
                </label>
                <div class="col-md-10 offset-md-1">
                    <select id="rol" class="custom-select" name="rol">
                        <option selected>Presione para seleccionar...</option>
                        <option value="admin">Administrador</option>
                        <option value="user">Operario</option>
                    </select>
                </div>
                <br>
                <div class="form-group row mb-0">
                    <div class="col-md-5 offset-md-1">
                        <button type="submit" class="btn btn-light btn-block">
                            {{ __('Registrar usuario') }}
                        </button>
                    </div>
                    <div class="col-md-5">
                        <a class="btn btn-light btn-block" href="/" role="button">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection