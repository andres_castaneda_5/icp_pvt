@extends('layouts.auth')

@section('content')

<div class="container">
    <br>
    <br>
    <br>
    <br>
    <div class="row justify-content-center">
        <div class="col-md-4 shadow p-3 mb-5 bg-white rounded">
            <div class="row">
                <div class="col">
                    <img src="../images/login.png" class="float-right" width="35" height="35">
                </div>
                <div class="col">
                    <center>
                        <h2>Ingresar</h2>
                    </center>
                </div>
                <div class="col"></div>
            </div>

            <hr>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <label for="correo" class="offset-md-1">
                    <strong>Correo electrónico:</strong>
                </label>
                <div class="col-md-10 offset-md-1">
                    <input id="correo" type="text" class="form-control @error('correo') is-invalid @enderror" name="correo" value="{{ old('correo') }}" required autocomplete="correo" autofocus placeholder="Digite su correo electrónico">
                    @error('correo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <br>
                <label for="password" class="offset-md-1">
                    <strong>Contraseña:</strong>
                </label>
                <div class="col-md-10 offset-md-1">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Digite su contraseña">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <br>
                <div class="form-group row mb-0">
                    <div class="col-md-5 offset-md-1">
                        <button type="submit" class="btn btn-light btn-block">
                            {{ __('Iniciar sesión') }}
                        </button>
                    </div>

                    <div class="col-md-5">
                        <a class="btn btn-light btn-block" href="/" role="button">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection