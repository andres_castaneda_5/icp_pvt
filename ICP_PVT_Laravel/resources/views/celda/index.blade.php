@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-3">
        <div class="card">
            <div class="card-header">
                <center>
                    <strong>
                        <h5>Equipos</h5>
                    </strong>
                </center>
            </div>
            <div class="list-group" id="menu">
                <a href="densimetro" class="list-group-item list-group-item-action">Densímetros</a>
                <a href="viscosimetro" class="list-group-item list-group-item-action">Viscosímetros</a>
                <a href="gasometro" class="list-group-item list-group-item-action">Gasómetros</a>
                <a href="celda" class="list-group-item list-group-item-action">Celdas</a>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="card">
            <div class="card-header">
                <center>
                    <h5>Celdas</h5>
                </center>
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>
                                <center>Código</center>
                            </th>
                            <th>
                                <center>Modelo</center>
                            </th>
                            <th>
                                <center>Funciones</center>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($celdas as $celda)
                        <tr>
                            <th>
                                <center>{{$celda->codigo}}</center>
                            </th>
                            <td>
                                <center>{{$celda->modelo}}</center>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-2 offset-4">
                                        <form action="{{action('CeldaController@edit', ['id'=>$celda->id])}}" method="GET">
                                            <input type="image" src="../images/edit.png" width="20" height="20" role="button">
                                        </form>
                                    </div>
                                    <div class="col-md-2">
                                        <form action="{{action('CeldaController@destroy', ['id'=>$celda->id])}}" method="POST">
                                            {{csrf_field()}} {{method_field('DELETE')}}
                                            <input type="image" src="../images/delete.png" alt="Borrar" width="20" height="20" role="button">
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-sm-3 offset-8">
                        <center>
                            <a class="btn btn-light" href="{{action('CeldaController@create')}}" role="button">Agregar</a>
                        </center>
                    </div>
                    {!!$celdas->render()!!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection