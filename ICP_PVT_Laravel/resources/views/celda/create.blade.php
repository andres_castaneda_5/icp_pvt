@extends('layouts.auth')

@section('content')
<br>
<br>
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Agregar Celda') }}</div>
                <div class="card-body">
                    <form action="{{action('CeldaController@store')}}" method="post">
                        {{csrf_field()}}

                        <div class="form-group row">
                            <label for="codigo" class="col-md-4 col-form-label text-md-right">{{ __('Código') }}</label>

                            <div class="col-md-6">
                                <input id="codigo" type="number" class="form-control" name="codigo">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Modelo') }}</label>

                            <div class="col-md-6">
                                <input id="modelo" type="text" class="form-control" name="modelo">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-3 offset-md-4">
                                <button type="submit" class="btn btn-light btn-block">
                                    {{ __('Guardar') }}
                                </button>
                            </div>

                            <div class="col-md-3">
                                <a class="btn btn-light btn-block" href="{{action('CeldaController@index')}}" role="button">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection