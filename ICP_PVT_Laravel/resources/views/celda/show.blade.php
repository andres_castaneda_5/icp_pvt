@extends('layouts.auth')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-9 shadow p-3 mb-5 bg-white rounded">
        <div class="row">
            <div class="col">
                <center>
                    <strong>
                        <h2>Celda</h2>
                    </strong>
                </center>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-4">
                <center>
                    <strong>
                        Código
                    </strong>
                </center>
            </div>
            <div class="col-sm-4">
                <center>
                    <strong>
                        Modelo
                    </strong>
                </center>
            </div>
            <div class="col-sm-4">
                <center>
                    <strong>
                        Funciones
                    </strong>
                </center>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <center>
                    {{$celda->codigo}}
                </center>
            </div>
            <div class="col-sm-4">
                <center>
                    {{$celda->modelo}}
                </center>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col">
                        <form action="{{action('CeldaController@edit', ['id'=>$celda->id])}}" method="GET">
                            <center>
                                <input type="image" src="../images/edit.png" width="25" height="25" role="button">
                            </center>
                        </form>
                    </div>
                    <div class="col">
                        <form action="{{action('CeldaController@destroy',['id'=>$celda->id])}}" method="POST">
                            {{csrf_field()}} {{method_field('DELETE')}}
                            <center>
                                <input type="image" src="../images/delete.png" alt="Borrar" width="25" height="25" role="button">
                            </center>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <br>
        <div class="row">
            <div class="col">
                <center>
                    <strong>
                        <h2>Análisis realizados</h2>
                    </strong>
                </center>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col">
                <center>
                    <strong>Pozo</strong>
                </center>
            </div>
            <div class="col">
                <center>
                    <strong>Fecha</strong>
                </center>
            </div>
            <div class="col">
                <center>
                    <strong>Presión</strong>
                </center>
            </div>
            <div class="col">
                <center>
                    <strong>Temperatura</strong>
                </center>
            </div>
            <div class="col">
                <center>
                    <strong>Volumen</strong>
                </center>
            </div>
            <div class="col">
                <center>
                    <strong>Observaciones</strong>
                </center>
            </div>
        </div>
        <br>
        @foreach($celda->pozos as $pozo)
        <div class="row">
        <div class="col">
                <center>{{$pozo->nombre}}</center>
            </div>
            <div class="col">
                <center></center>
            </div>
            <div class="col">
                <center></center>
            </div>
            <div class="col">
                <center></center>
            </div>
            <div class="col">
                <center></center>
            </div>
            <div class="col">
                <center></center>
            </div>
        </div>
        @endforeach
        <br>
        <div class="row">
            <div class="col-sm-2">
                <center>
                    <a class="btn btn-light" href="{{action('CeldaController@index')}}" role="button">Volver</a>
                </center>
            </div>
        </div>
    </div>
</div>

@endsection