<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>Ecopetrol</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    
    <link rel="shortcut icon" type="image/x-icon" href="/images/logo.png" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        body {
            background: url('../images/fondo.png') no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .navbar {
            background-color: #C0CC4A;
        }

        .card {
            box-shadow: 2px 2px 10px #666;
        }

        .card-header {
            background-color: #00483A;
            color: #FFFFFF
        }

        #logout {
            color: red;
        }

        .btn {
            background-color: #00483A;
            color: #FFFFFF;
        }

        .button {
            background-color: #C0CC4A;
        }

        .nav-link {
            color: #00483A
        }

        #add {
            float: right;
        }

        #encabezado {
            background-color: #00483A;
            color: #FFFFFF;
        }

        #equipos {
            background-color: #C0CC4A;
            color: #00483A;
        }

        #menu {
            background-color: #00483A;
        }

        #buscar {
            background-color: #FFFFFF;
        }
    </style>
</head>

<script type="text/javascript">
    function ConfirmDelete() {
        var respuesta = confirm("¿Estás seguro que deseas eliminar el registro?");
        if (respuesta == true) {
            return true;
        } else {
            return false;
        }
    }
</script>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-red shadow-sm">
            <div class="container">
                @if (Auth()->user()->rol == 'admin')
                <a class="navbar-brand" href="{{ url('/admin') }}">
                    Ecopetrol
                </a>
                @else
                <a class="navbar-brand" href="{{ url('/home') }}">
                    Ecopetrol
                </a>
                @endif
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>
                    @guest
                    @if (Route::has('login'))

                    @endif
                    @else
                    <ul class="nav">
                        @if (Auth()->user()->rol == 'admin')
                        <li class="nav-item">
                            <a class="nav-link active" href="usuario">Usuarios</a>
                        </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link active" href="pozo">Pozos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="muestra">Muestras</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Equipos</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="/densimetro">Densímetros</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/viscosimetro">Viscosímetros</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/gasometro">Gasómetros</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/celda">Celdas</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Procedimientos</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="/densimetro">Densidad</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/viscosimetro">Viscosidad</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/gasometro">Expansión Flash</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/celda">CCE</a>
                                <a class="dropdown-item" href="/celda">DL</a>
                                <a class="dropdown-item" href="/celda">SDS</a>
                            </div>
                        </li>
                    </ul>
                    @endguest
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Iniciar Sesión') }}</a>
                        </li>
                        @if (Route::has('login'))
                        <!-- <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Registrarse') }}</a>
                        </li> -->
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->nombre }} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" id="logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Cerrar Sesión') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <main class="py-3">
        <div class="col-md-10 offset-1">
            @yield('content')
        </div>
    </main>
</body>

</html>