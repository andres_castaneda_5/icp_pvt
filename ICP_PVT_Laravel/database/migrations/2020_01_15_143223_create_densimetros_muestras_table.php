<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDensimetrosMuestrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('densimetros_muestras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo')->unique();
            $table->date('fecha');
            $table->double('temperatura');
            $table->double('densidad1');
            $table->double('densidad2');
            $table->double('densidad3');
            $table->double('densidad4');
            $table->double('densidad5');
            $table->double('densidad6');
            $table->double('densidad_prom');
            $table->double('api');
            $table->double('agua');
            $table->string('observaciones');
            $table->unsignedBigInteger('id_usuario');
            $table->foreign('id_usuario')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('id_densimetro');
            $table->foreign('id_densimetro')->references('id')->on('densimetros')->onDelete('cascade');
            $table->unsignedBigInteger('id_muestra');
            $table->foreign('id_muestra')->references('id')->on('muestras')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('densimetros_muestras');
    }
}
