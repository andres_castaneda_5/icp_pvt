<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViscosimetrosMuestrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viscosimetros_muestras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo')->unique();
            $table->date('fecha');
            $table->double('presion');
            $table->double('viscosidad');
            $table->double('pcv');
            $table->double('temperatura');
            $table->double('ciclo');
            $table->double('desviacion');
            $table->string('observaciones');
            $table->unsignedBigInteger('id_usuario');
            $table->foreign('id_usuario')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('id_viscosimetro');
            $table->foreign('id_viscosimetro')->references('id')->on('viscosimetros')->onDelete('cascade');
            $table->unsignedBigInteger('id_muestra');
            $table->foreign('id_muestra')->references('id')->on('muestras')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viscosimetros_muestras');
    }
}
