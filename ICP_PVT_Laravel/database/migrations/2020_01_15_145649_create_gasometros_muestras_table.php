<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGasometrosMuestrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gasometros_muestras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo')->unique();
            $table->date('fecha');
            $table->double('presion');
            $table->double('temperatura');
            $table->double('volumen1');
            $table->double('volumen2');
            $table->double('peso1');
            $table->double('peso2');
            $table->double('peso3');
            $table->string('observaciones');
            $table->unsignedBigInteger('id_usuario');
            $table->foreign('id_usuario')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('id_gasometro');
            $table->foreign('id_gasometro')->references('id')->on('gasometros')->onDelete('cascade');
            $table->unsignedBigInteger('id_muestra');
            $table->foreign('id_muestra')->references('id')->on('muestras')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gasometros_muestras');
    }
}
