<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePozosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pozos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('submission')->unique();
            $table->string('nombre');
            $table->enum('tipo_muestra',['Fondo', 'Superficie']);
            $table->string('punto_muestreo');
            $table->string('formacion_productora');
            $table->double('profundidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pozos');
    }
}
