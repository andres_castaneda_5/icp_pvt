<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMuestrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('muestras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('submission')->unique();
            $table->string('nombre');
            $table->enum('tipo_muestra',['Fondo', 'Superficie']);
            $table->string('formacion_productora');
            $table->double('profundidad');
            $table->double('presion');
            $table->double('volumen');
            $table->double('temperatura');
            $table->double('contenido_agua');
            $table->unsignedBigInteger('id_pozo');
            $table->foreign('id_pozo')->references('id')->on('pozos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('muestras');
    }
}
