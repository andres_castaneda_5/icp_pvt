<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCeldasMuestrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('celdas_muestras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo')->unique();
            $table->date('fecha');
            $table->double('presion');
            $table->double('temperatura');
            $table->double('volumen');
            $table->string('observaciones');
            $table->unsignedBigInteger('id_usuario');
            $table->foreign('id_usuario')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('id_celda');
            $table->foreign('id_celda')->references('id')->on('gasometros')->onDelete('cascade');
            $table->unsignedBigInteger('id_muestra');
            $table->foreign('id_muestra')->references('id')->on('muestras')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('celdas_muestras');
    }
}
