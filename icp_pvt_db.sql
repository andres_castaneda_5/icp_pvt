-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.11-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para icp_pvt_db
CREATE DATABASE IF NOT EXISTS `icp_pvt_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `icp_pvt_db`;

-- Volcando estructura para tabla icp_pvt_db.audits
CREATE TABLE IF NOT EXISTS `audits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `event` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auditable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auditable_id` bigint(20) unsigned NOT NULL,
  `old_values` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_values` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(1023) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `audits_auditable_type_auditable_id_index` (`auditable_type`,`auditable_id`),
  KEY `audits_user_id_user_type_index` (`user_id`,`user_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla icp_pvt_db.celdas
CREATE TABLE IF NOT EXISTS `celdas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modelo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `celdas_codigo_unique` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla icp_pvt_db.celdas_muestras
CREATE TABLE IF NOT EXISTS `celdas_muestras` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `presion` double NOT NULL,
  `temperatura` double NOT NULL,
  `volumen` double NOT NULL,
  `observaciones` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_usuario` bigint(20) unsigned NOT NULL,
  `id_celda` bigint(20) unsigned NOT NULL,
  `id_muestra` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `celdas_muestras_codigo_unique` (`codigo`),
  KEY `celdas_muestras_id_usuario_foreign` (`id_usuario`),
  KEY `celdas_muestras_id_celda_foreign` (`id_celda`),
  KEY `celdas_muestras_id_muestra_foreign` (`id_muestra`),
  CONSTRAINT `celdas_muestras_id_celda_foreign` FOREIGN KEY (`id_celda`) REFERENCES `gasometros` (`id`) ON DELETE CASCADE,
  CONSTRAINT `celdas_muestras_id_muestra_foreign` FOREIGN KEY (`id_muestra`) REFERENCES `muestras` (`id`) ON DELETE CASCADE,
  CONSTRAINT `celdas_muestras_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla icp_pvt_db.densimetros
CREATE TABLE IF NOT EXISTS `densimetros` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modelo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `densimetros_codigo_unique` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla icp_pvt_db.densimetros_muestras
CREATE TABLE IF NOT EXISTS `densimetros_muestras` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `temperatura` double NOT NULL,
  `densidad1` double NOT NULL,
  `densidad2` double NOT NULL,
  `densidad3` double NOT NULL,
  `densidad4` double NOT NULL,
  `densidad5` double NOT NULL,
  `densidad6` double NOT NULL,
  `densidad_prom` double NOT NULL,
  `api` double NOT NULL,
  `agua` double NOT NULL,
  `observaciones` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_usuario` bigint(20) unsigned NOT NULL,
  `id_densimetro` bigint(20) unsigned NOT NULL,
  `id_muestra` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `densimetros_muestras_codigo_unique` (`codigo`),
  KEY `densimetros_muestras_id_usuario_foreign` (`id_usuario`),
  KEY `densimetros_muestras_id_densimetro_foreign` (`id_densimetro`),
  KEY `densimetros_muestras_id_muestra_foreign` (`id_muestra`),
  CONSTRAINT `densimetros_muestras_id_densimetro_foreign` FOREIGN KEY (`id_densimetro`) REFERENCES `densimetros` (`id`) ON DELETE CASCADE,
  CONSTRAINT `densimetros_muestras_id_muestra_foreign` FOREIGN KEY (`id_muestra`) REFERENCES `muestras` (`id`) ON DELETE CASCADE,
  CONSTRAINT `densimetros_muestras_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla icp_pvt_db.gasometros
CREATE TABLE IF NOT EXISTS `gasometros` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modelo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `gasometros_codigo_unique` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla icp_pvt_db.gasometros_muestras
CREATE TABLE IF NOT EXISTS `gasometros_muestras` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `presion` double NOT NULL,
  `temperatura` double NOT NULL,
  `volumen1` double NOT NULL,
  `volumen2` double NOT NULL,
  `peso1` double NOT NULL,
  `peso2` double NOT NULL,
  `peso3` double NOT NULL,
  `observaciones` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_usuario` bigint(20) unsigned NOT NULL,
  `id_gasometro` bigint(20) unsigned NOT NULL,
  `id_muestra` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `gasometros_muestras_codigo_unique` (`codigo`),
  KEY `gasometros_muestras_id_usuario_foreign` (`id_usuario`),
  KEY `gasometros_muestras_id_gasometro_foreign` (`id_gasometro`),
  KEY `gasometros_muestras_id_muestra_foreign` (`id_muestra`),
  CONSTRAINT `gasometros_muestras_id_gasometro_foreign` FOREIGN KEY (`id_gasometro`) REFERENCES `gasometros` (`id`) ON DELETE CASCADE,
  CONSTRAINT `gasometros_muestras_id_muestra_foreign` FOREIGN KEY (`id_muestra`) REFERENCES `muestras` (`id`) ON DELETE CASCADE,
  CONSTRAINT `gasometros_muestras_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla icp_pvt_db.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla icp_pvt_db.muestras
CREATE TABLE IF NOT EXISTS `muestras` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `submission` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_muestra` enum('Fondo','Superficie') COLLATE utf8mb4_unicode_ci NOT NULL,
  `formacion_productora` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profundidad` double NOT NULL,
  `presion` double NOT NULL,
  `volumen` double NOT NULL,
  `temperatura` double NOT NULL,
  `contenido_agua` double NOT NULL,
  `id_pozo` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `muestras_submission_unique` (`submission`),
  KEY `muestras_id_pozo_foreign` (`id_pozo`),
  CONSTRAINT `muestras_id_pozo_foreign` FOREIGN KEY (`id_pozo`) REFERENCES `pozos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla icp_pvt_db.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla icp_pvt_db.pozos
CREATE TABLE IF NOT EXISTS `pozos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `submission` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_muestra` enum('Fondo','Superficie') COLLATE utf8mb4_unicode_ci NOT NULL,
  `punto_muestreo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `formacion_productora` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profundidad` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pozos_submission_unique` (`submission`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla icp_pvt_db.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `documento` int(11) NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rol` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_documento_unique` (`documento`),
  UNIQUE KEY `users_correo_unique` (`correo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla icp_pvt_db.viscosimetros
CREATE TABLE IF NOT EXISTS `viscosimetros` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modelo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `viscosimetros_codigo_unique` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla icp_pvt_db.viscosimetros_muestras
CREATE TABLE IF NOT EXISTS `viscosimetros_muestras` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `presion` double NOT NULL,
  `viscosidad` double NOT NULL,
  `pcv` double NOT NULL,
  `temperatura` double NOT NULL,
  `ciclo` double NOT NULL,
  `desviacion` double NOT NULL,
  `observaciones` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_usuario` bigint(20) unsigned NOT NULL,
  `id_viscosimetro` bigint(20) unsigned NOT NULL,
  `id_muestra` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `viscosimetros_muestras_codigo_unique` (`codigo`),
  KEY `viscosimetros_muestras_id_usuario_foreign` (`id_usuario`),
  KEY `viscosimetros_muestras_id_viscosimetro_foreign` (`id_viscosimetro`),
  KEY `viscosimetros_muestras_id_muestra_foreign` (`id_muestra`),
  CONSTRAINT `viscosimetros_muestras_id_muestra_foreign` FOREIGN KEY (`id_muestra`) REFERENCES `muestras` (`id`) ON DELETE CASCADE,
  CONSTRAINT `viscosimetros_muestras_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `viscosimetros_muestras_id_viscosimetro_foreign` FOREIGN KEY (`id_viscosimetro`) REFERENCES `viscosimetros` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
